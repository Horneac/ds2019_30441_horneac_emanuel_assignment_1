package com.horneacemanuel.assignment1.controller;

import com.horneacemanuel.assignment1.DTO.MedicationDTO;
import com.horneacemanuel.assignment1.services.MedicationService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/medication")
@RequiredArgsConstructor
public class MedicationController {
    private final MedicationService medicationService;

    @DeleteMapping(value = "/{id}")
    public Integer delete(@PathVariable("id") Integer id){
        return medicationService.delete(id);
    }
    @GetMapping()
    public List<MedicationDTO> findAll(){
        return medicationService.findAll();
    }

    @GetMapping(value = "/{id}")
    public MedicationDTO findById(@PathVariable("id") Integer id){
        return medicationService.findById(id);
    }
    @PostMapping()
    public Integer insert(@RequestBody MedicationDTO medicationDTO){
        return medicationService.insert(medicationDTO);
    }

    @PutMapping()
    public MedicationDTO update(@RequestBody MedicationDTO medicationDTO){
        return medicationService.update(medicationDTO);
    }
}
