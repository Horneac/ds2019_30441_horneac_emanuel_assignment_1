package com.horneacemanuel.assignment1.controller;

import com.horneacemanuel.assignment1.DTO.UserDTO;
import com.horneacemanuel.assignment1.DTO.UserViewDTO;
import com.horneacemanuel.assignment1.entity.Role;
import com.horneacemanuel.assignment1.services.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/user")
@RequiredArgsConstructor
public class UserController {
    private final UserService userService;

    @GetMapping(value = "/{id}")
    public UserDTO findById(@PathVariable("id") Integer id){
        return userService.findById(id);
    }

    @GetMapping()
    public List<UserDTO> findAll(){
        return userService.findAll();
    }

    @PostMapping()
    public Integer insertUserDTO(@RequestBody UserDTO userDTO){
        return userService.insert(userDTO);
    }

    @PutMapping()
    public UserViewDTO updateUser(@RequestBody UserDTO userDTO){
        return userService.update(userDTO);
    }

    @DeleteMapping(value = "/{id}")
    public Integer deleteById(@PathVariable("id") Integer id){
        userService.deleteById(id);
        return id;
    }

    @PutMapping(value = "/login")
    public Role login(){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        System.out.println("-----------------------------");
        if(!auth.getAuthorities().isEmpty()){
            Role role = Role.valueOf(auth.getAuthorities().toArray()[0].toString());
            System.out.println("-----------------------------");
            System.out.println(role.toString());
            return role;
        }
        return Role.NONE;
    }

    //for caregivers
    @PostMapping(value = "/caregiver")
    public Integer insertCaregiver(@RequestBody UserDTO userDTO){
        return userService.insert(userDTO);
    }
    @GetMapping(value = "/caregiver")
    public List<UserViewDTO> findAllCaregivers(){
        return userService.findByRole(Role.CAREGIVER);
    }

    @GetMapping(value = "/caregiver/{id}")
    public UserViewDTO findCaregiverById(@PathVariable("id") Integer id){
        return userService.findCaregiverById(id);
    }

    @PutMapping(value = "/caregiver")
    public Integer updateCaregiver(@RequestBody UserDTO userDTO){
        return userService.updateCaregiver(userDTO);
    }

    @DeleteMapping(value = "/caregiver/{id}")
    public Integer deleteCaregiver(@PathVariable("id") Integer id){
        return userService.deleteCaregiverById(id);
    }
}
