package com.horneacemanuel.assignment1.controller;

import com.horneacemanuel.assignment1.DTO.MedicationPlanDTO;
import com.horneacemanuel.assignment1.services.MedicationPlanService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/medicationPlan")
@RequiredArgsConstructor
public class MedicationPlanController {
    private final MedicationPlanService medicationPlanService;

    @PostMapping()
    public Integer insert(@RequestBody MedicationPlanDTO medicationPlanDTO){
        return medicationPlanService.insert(medicationPlanDTO);
    }

    @GetMapping()
    public List<MedicationPlanDTO> findAll(){
        return medicationPlanService.findAll();
    }

    @GetMapping(value = "/{username}")
    public List<MedicationPlanDTO> findAllByUsername(@PathVariable("username") String username){
        return medicationPlanService.findAllByUsername(username);
    }
}
