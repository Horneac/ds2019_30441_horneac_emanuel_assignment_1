package com.horneacemanuel.assignment1.controller;

import com.horneacemanuel.assignment1.DTO.PatientDTO;
import com.horneacemanuel.assignment1.DTO.PatientViewDTO;
import com.horneacemanuel.assignment1.services.PatientService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping(value = "/patient")
@RequiredArgsConstructor
public class PatientController {
    private final PatientService patientService;

    //deletes patients based on id received as path variable
    @DeleteMapping(value = "/{id}")
    public Boolean delete(@PathVariable("id") Integer id){
        patientService.deleteById(id);
        return true;
    }

    //update a patient
    @PutMapping
    public Integer update(@RequestBody PatientDTO patientDTO){
        return patientService.update(patientDTO);
    }
    //returns all patients
    @GetMapping()
    public List<PatientViewDTO> findAll(){
        return patientService.findAll();
    }

    //inserts a patient from a PatientDTO received
    @PostMapping
    public Integer insert(@RequestBody PatientDTO patientDTO){
        return patientService.insert(patientDTO);
    }

    @GetMapping(value = "/{name}")
    public List<PatientViewDTO> findAllByCaregiverName(@PathVariable("name") String name){
        return patientService.findByCaregiverName(name);
    }

    @GetMapping(value = "/info/{username}")
    public PatientViewDTO findByUsername(@PathVariable("username") String username){
        return patientService.findByUsername(username);
    }
}
