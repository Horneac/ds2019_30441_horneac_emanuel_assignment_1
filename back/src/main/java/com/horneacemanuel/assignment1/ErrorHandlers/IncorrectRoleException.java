package com.horneacemanuel.assignment1.ErrorHandlers;

import com.horneacemanuel.assignment1.entity.Role;
import org.springframework.http.HttpStatus;

public class IncorrectRoleException extends RuntimeException {
    public static final HttpStatus HTTP_STATUS =  HttpStatus.FORBIDDEN;
    private final String resourceName;

    public IncorrectRoleException(String resourceName, Role expectedRole, Role actualRole) {
        super(String.format("%s is not actually a '%s', it is a '%s", resourceName, expectedRole, actualRole));
        this.resourceName = resourceName;

    }

    public String getResourceName() {
        return resourceName;
    }
}
