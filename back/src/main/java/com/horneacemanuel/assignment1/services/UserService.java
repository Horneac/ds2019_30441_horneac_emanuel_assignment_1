package com.horneacemanuel.assignment1.services;

import com.horneacemanuel.assignment1.DTO.UserDTO;
import com.horneacemanuel.assignment1.DTO.UserViewDTO;
import com.horneacemanuel.assignment1.DTO.builders.UserBuilder;
import com.horneacemanuel.assignment1.ErrorHandlers.DuplicateEntryException;
import com.horneacemanuel.assignment1.ErrorHandlers.IncorrectRoleException;
import com.horneacemanuel.assignment1.ErrorHandlers.ResourceNotFoundException;
import com.horneacemanuel.assignment1.entity.Role;
import com.horneacemanuel.assignment1.entity.User;
import com.horneacemanuel.assignment1.repositories.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class UserService {
    private final UserRepository userRepository;

    @Transactional
    public UserDTO findById(Integer Id){

        Optional<User> user = userRepository.findById(Id);

        if(!user.isPresent()){
            throw new ResourceNotFoundException("User","user id", Id);
        }
        return UserBuilder.generateDTOFromEntity(user.get());
    }

    @Transactional
    public List<UserDTO> findAll(){
        List<User> users = userRepository.findAll();

        return users.stream()
                .map(UserBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());
    }

    @Transactional
    public Integer delete(UserDTO userDTO){
        userRepository.deleteById(userDTO.getId());
        return userDTO.getId();
    }
    @Transactional
    public Integer insert(UserDTO userDTO){
        User user = userRepository.findByUsername(userDTO.getUsername());
        System.out.println(user);
        System.out.println("=----------------------hereeeeeeeeeeeeeeeee-----------------");
        if(user != null){
            throw new DuplicateEntryException("user","username",userDTO.getUsername());
        }
        user = UserBuilder.generateEntityFromDTO(userDTO);
        user.setPassword(passwordEncoder().encode(user.getPassword()));
        return userRepository.save(user).getId();
    }

    @Transactional
    public UserViewDTO update(UserDTO userDTO){
        Optional<User> user = userRepository.findById(userDTO.getId());

        if(!user.isPresent()){
            throw new ResourceNotFoundException("User","id",userDTO.getId());
        }

        User user1 = userRepository.save(UserBuilder.generateEntityFromDTO(userDTO));
        return UserBuilder.generateViewDTOFromEntity(user1);
    }

    @Transactional
    public User findByUsername(String username){
        return userRepository.findByUsername(username);
    }

    @Transactional
    public void deleteById(Integer id){
        userRepository.deleteById(id);
    }

    @Transactional
    public List<UserViewDTO> findByRole(Role role){
        return userRepository.findAllByRole(role).stream()
                .map(UserBuilder::generateViewDTOFromEntity)
                .collect(Collectors.toList());
    }

    @Transactional
    public UserViewDTO findCaregiverById(Integer id){
       User user = userRepository.findByIdAndRole(id, Role.CAREGIVER);

       return UserBuilder.generateViewDTOFromEntity(user);
    }

    @Transactional
    public Integer deleteCaregiverById(Integer id){
        User user = userRepository.findByIdAndRole(id, Role.CAREGIVER);
        userRepository.delete(user);
        return user.getId();
    }

    @Transactional
    public Integer updateCaregiver(UserDTO userDTO){
        User user = UserBuilder.generateEntityFromDTO(userDTO);
        if(!user.getRole().equals(Role.CAREGIVER)){
            throw new IncorrectRoleException("user's role",Role.CAREGIVER,user.getRole());
        }
        user = userRepository.save(user);
        return user.getId();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
