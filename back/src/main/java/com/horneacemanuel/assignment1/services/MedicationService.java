package com.horneacemanuel.assignment1.services;

import com.horneacemanuel.assignment1.DTO.MedicationDTO;
import com.horneacemanuel.assignment1.DTO.builders.MedicationBuilder;
import com.horneacemanuel.assignment1.ErrorHandlers.ResourceNotFoundException;
import com.horneacemanuel.assignment1.entity.Medication;
import com.horneacemanuel.assignment1.repositories.MedicationRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class MedicationService {
    private final MedicationRepository medicationRepository;

    @Transactional
    public Integer insert(MedicationDTO medicationDTO){
        return medicationRepository.save(MedicationBuilder.generateEntityFromDTO(medicationDTO)).getId();
    }

    @Transactional
    public MedicationDTO findById(Integer id){
        Optional<Medication> medicaation = medicationRepository.findById(id);

        if(!medicaation.isPresent()){
            throw new ResourceNotFoundException("medication","id", id);
        }
        return MedicationBuilder.generateDTOFromEntity(medicaation.get());
    }

    @Transactional
    public List<MedicationDTO> findAll(){
        List<Medication> medications = medicationRepository.findAll();

        return medications.stream()
                .map(MedicationBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());
    }

    @Transactional
    public MedicationDTO update(MedicationDTO medicationDTO){
        Medication medication = medicationRepository.findByName(medicationDTO.getName());

        Medication newMedication = MedicationBuilder.generateEntityFromDTO(medicationDTO,medication.getMedicationsPerPlan());
        newMedication = medicationRepository.save(newMedication);

        return MedicationBuilder.generateDTOFromEntity(newMedication);

    }

    @Transactional
    public Integer delete(Integer id){
        medicationRepository.deleteById(id);
        return id;
    }
}
