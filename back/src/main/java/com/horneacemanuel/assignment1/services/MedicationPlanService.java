package com.horneacemanuel.assignment1.services;

import com.horneacemanuel.assignment1.DTO.MedicationPlanDTO;
import com.horneacemanuel.assignment1.DTO.builders.MedicationPlanBuilder;
import com.horneacemanuel.assignment1.entity.Medication;
import com.horneacemanuel.assignment1.entity.MedicationPerPlan;
import com.horneacemanuel.assignment1.entity.MedicationPlan;
import com.horneacemanuel.assignment1.entity.User;
import com.horneacemanuel.assignment1.repositories.MedicationPerPlanRepository;
import com.horneacemanuel.assignment1.repositories.MedicationPlanRepository;
import com.horneacemanuel.assignment1.repositories.MedicationRepository;
import com.horneacemanuel.assignment1.repositories.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class MedicationPlanService {
    private final MedicationPlanRepository      medicationPlanRepository;
    private final MedicationRepository          medicationRepository;
    private final MedicationPerPlanRepository   medicationPerPlanRepository;
    private final UserRepository                userRepository;

    @Transactional
    public Integer insert(MedicationPlanDTO medicationPlanDTO) {
        System.out.println(medicationPlanDTO);
        MedicationPlan medicationPlan = new MedicationPlan();
        medicationPlan = medicationPlanRepository.save(medicationPlan);
        List<MedicationPerPlan> medicationPerPlanaList = new ArrayList<>();
        String[] medications = medicationPlanDTO.getMedications().split(" ");
        System.out.println(medications[0]);
        User patient = userRepository.findByUsername(medicationPlanDTO.getUsername());
        for (String s : medications) {
            String[] attributes = s.split("-");
            String medicationName = attributes[0];
            String dosage = attributes[1];
            String intakeMoments = "";
            for (int i = 2; i < attributes.length; i++) {
                intakeMoments.concat(attributes[i] + "-");
            }
            Medication medication = medicationRepository.findByName(medicationName);
            MedicationPerPlan medicationPerPlan = new MedicationPerPlan(
                    0,
                    medication,
                    medicationPlan,
                    Double.parseDouble(dosage),
                    intakeMoments
            );
            medicationPerPlan.setId(medicationPerPlanRepository.save(medicationPerPlan).getId());

            medicationPerPlanaList.add(medicationPerPlan);
        }

        medicationPlan.setEndTime(medicationPlanDTO.getEndTime());
        medicationPlan.setStartTime(medicationPlanDTO.getStartTime());
        medicationPlan.setPatient(patient);
        medicationPlan.setMedicationsPerPlan(medicationPerPlanaList);

        return medicationPlanRepository.save(medicationPlan).getId();
    }

    @Transactional
    public List<MedicationPlanDTO> findAll(){
        List<MedicationPlan> medicationPlans = medicationPlanRepository.findAll();

        return medicationPlans.stream()
                .map(MedicationPlanBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());
    }

    @Transactional
    public List<MedicationPlanDTO> findAllByUsername(String username){
        List<MedicationPlan> medicationPlans = medicationPlanRepository.findAllByPatientUsername(username);

        return medicationPlans.stream()
                .map(MedicationPlanBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());
    }
}
