package com.horneacemanuel.assignment1.services;

import com.horneacemanuel.assignment1.DTO.PatientDTO;
import com.horneacemanuel.assignment1.DTO.PatientViewDTO;
import com.horneacemanuel.assignment1.DTO.UserDTO;
import com.horneacemanuel.assignment1.DTO.builders.PatientViewBuilder;
import com.horneacemanuel.assignment1.DTO.builders.UserBuilder;
import com.horneacemanuel.assignment1.ErrorHandlers.IncorrectRoleException;
import com.horneacemanuel.assignment1.ErrorHandlers.ResourceNotFoundException;
import com.horneacemanuel.assignment1.entity.Patient;
import com.horneacemanuel.assignment1.entity.Role;
import com.horneacemanuel.assignment1.entity.User;
import com.horneacemanuel.assignment1.repositories.PatientRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class PatientService {
    private final UserService userService;
    private final PatientRepository patientRepository;

    public Integer insert(PatientDTO patientDTO){
        Patient patient = generateEntityFromDTO(patientDTO);

        Integer newId = userService.insert(UserBuilder.generateDTOFromEntity(patient.getUser()));
        patient.getUser().setId(newId);

        return patientRepository.save(patient).getId();
    }

    public void deleteById(Integer id){
        Optional<Patient> patient = patientRepository.findById(id);

        if(!patient.isPresent()){
            throw new ResourceNotFoundException("patient","id",id);
        }

        User user = patient.get().getUser();

        patientRepository.delete(patient.get());

        userService.delete(UserBuilder.generateDTOFromEntity(user));
    }

    public List<PatientViewDTO> findAll(){
        return patientRepository.findAll().stream()
                .map(PatientViewBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());
    }

    public Integer update(PatientDTO patientDTO){
        Patient patient = generateEntityFromDTO(patientDTO);
        User user = patient.getUser();
        user.setId(userService.findByUsername(user.getUsername()).getId());

        userService.update(UserBuilder.generateDTOFromEntity(user));

        return patientRepository.save(patient).getId();

    }

    public List<PatientViewDTO> findByCaregiverName(String name){
        List<Patient> patients = patientRepository.findAllByCaregiverName(name);
        return patients.stream()
                .map(PatientViewBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());
    }

    public PatientViewDTO findByUsername(String username){
        return PatientViewBuilder.generateDTOFromEntity(patientRepository.findByUserUsername(username));
    }

    public Patient generateEntityFromDTO(PatientDTO patientDTO){
        Patient patient = new Patient();
        User doctor = UserBuilder.generateEntityFromDTO(
                userService.findById(patientDTO.getDoctorId())
        );
        if(!doctor.getRole().equals(Role.DOCTOR)){
            throw new IncorrectRoleException("patient's doctor",Role.DOCTOR, doctor.getRole());
        }
        User caregiver = UserBuilder.generateEntityFromDTO(
                userService.findById(patientDTO.getCaregiverId())
        );
        if(!caregiver.getRole().equals(Role.CAREGIVER)){
            throw new IncorrectRoleException("patient's caregiver",Role.CAREGIVER, caregiver.getRole());
        }
        UserDTO userDTO = patientDTO.getUser();

        patient.setCaregiver(caregiver);
        patient.setDoctor(doctor);
        patient.setMedicalRecord(patientDTO.getMedicalRecord());
        patient.setUser(UserBuilder.generateEntityFromDTO(userDTO));
        patient.setId(patientDTO.getId());
        return patient;
    }

}
