package com.horneacemanuel.assignment1.seed;

import com.horneacemanuel.assignment1.DTO.builders.PatientBuilder;
import com.horneacemanuel.assignment1.DTO.builders.UserBuilder;
import com.horneacemanuel.assignment1.entity.Patient;
import com.horneacemanuel.assignment1.entity.Role;
import com.horneacemanuel.assignment1.entity.User;
import com.horneacemanuel.assignment1.services.PatientService;
import com.horneacemanuel.assignment1.services.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
@RequiredArgsConstructor
@Order(Ordered.HIGHEST_PRECEDENCE)
public class UserSeed implements CommandLineRunner {
    private final UserService userService;
    private final PatientService patientService;
    private final PasswordEncoder passwordEncoder;

    @Override
    public void run(String... args) throws Exception {

        if (userService.findAll().isEmpty()) {
            User doctor = new User(0,"doctor","male", new Date(),"Ale. Crinului bloc c3/2","doctor",passwordEncoder.encode("doctor"), Role.DOCTOR);

            User caregiver = new User(0,"caregiver","male", new Date(),"Ale. Mosului bloc c3/2","caregiver",passwordEncoder.encode("caregiver"), Role.CAREGIVER);
            User patient = new User(0,"patient","female", new Date(),"Ale. Emanuel bloc c3/2","patient",passwordEncoder.encode("patient"), Role.PATIENT);

            Patient patient1 = new Patient(0,patient,doctor,caregiver,"ampicilina-02.10.2019");

            doctor.setId(userService.insert(UserBuilder.generateDTOFromEntity(doctor)));
            caregiver.setId(userService.insert(UserBuilder.generateDTOFromEntity(caregiver)));
            patientService.insert(PatientBuilder.generateDTOFromEntity(patient1));
        }
    }

}