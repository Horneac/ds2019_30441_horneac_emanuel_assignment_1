package com.horneacemanuel.assignment1.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

@Data
@Entity
@Table
@NoArgsConstructor
@AllArgsConstructor
public class MedicationPlan {
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn
    private User patient;

    @OneToMany
    @JoinColumn
    private List<MedicationPerPlan> medicationsPerPlan;

    private Date startTime;
    private Date endTime;
}
