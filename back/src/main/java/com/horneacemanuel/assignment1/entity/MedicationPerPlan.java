package com.horneacemanuel.assignment1.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table
public class MedicationPerPlan {
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn
    private Medication medication;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn
    private MedicationPlan medicationPlan;

    private Double dosage;

    private String intakeMoments;


}
