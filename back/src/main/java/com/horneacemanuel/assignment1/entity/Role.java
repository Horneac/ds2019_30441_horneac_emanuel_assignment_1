package com.horneacemanuel.assignment1.entity;


public enum Role {
    DOCTOR, PATIENT, CAREGIVER, NONE
}
