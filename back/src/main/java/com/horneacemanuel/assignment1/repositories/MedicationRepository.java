package com.horneacemanuel.assignment1.repositories;

import com.horneacemanuel.assignment1.entity.Medication;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MedicationRepository extends JpaRepository<Medication, Integer> {
    Medication findByName(String name);
}
