package com.horneacemanuel.assignment1.repositories;

import com.horneacemanuel.assignment1.entity.MedicationPerPlan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MedicationPerPlanRepository extends JpaRepository<MedicationPerPlan, Integer> {
}
