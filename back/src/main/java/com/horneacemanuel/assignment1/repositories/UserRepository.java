package com.horneacemanuel.assignment1.repositories;

import com.horneacemanuel.assignment1.entity.Role;
import com.horneacemanuel.assignment1.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

    User findByUsername(String username);

    List<User> findAllByRole(Role role);

    User findByIdAndRole(Integer id, Role role);
}
