package com.horneacemanuel.assignment1.repositories;

import com.horneacemanuel.assignment1.entity.MedicationPlan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MedicationPlanRepository extends JpaRepository<MedicationPlan, Integer> {
    List<MedicationPlan> findAllByPatientUsername(String username);
}
