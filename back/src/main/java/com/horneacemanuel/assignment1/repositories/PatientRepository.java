package com.horneacemanuel.assignment1.repositories;

import com.horneacemanuel.assignment1.entity.Patient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PatientRepository extends JpaRepository<Patient, Integer> {

    List<Patient> findAllByCaregiverName(String name);

    Patient findByUserUsername(String username);
}
