package com.horneacemanuel.assignment1.DTO.builders;

import com.horneacemanuel.assignment1.DTO.UserViewDTO;
import com.horneacemanuel.assignment1.entity.User;

public class UserViewBuilder {

    public static UserViewDTO GenerateDtoFromEntity(User user){
        return new UserViewDTO(
                user.getId(),
                user.getName(),
                user.getGender(),
                user.getDateOfBirth(),
                user.getAddress(),
                user.getUsername(),
                user.getRole()
        );
    }

    public static User GenerateEntityFromDTO(UserViewDTO userViewDTO){
        return new User(
                userViewDTO.getId(),
                userViewDTO.getName(),
                userViewDTO.getGender(),
                userViewDTO.getDateOfBirth(),
                userViewDTO.getAddress(),
                userViewDTO.getUsername(),
                "",
                userViewDTO.getRole()
        );
    }
}
