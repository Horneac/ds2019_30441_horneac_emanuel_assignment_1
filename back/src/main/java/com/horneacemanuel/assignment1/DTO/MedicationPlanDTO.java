package com.horneacemanuel.assignment1.DTO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MedicationPlanDTO {
    private Integer id;
    private String username;

    //ex: "ampicilina-dosage-intakeMoment1-intakeMoment2- penicilina
    private String medications;

    private Date startTime;

    private Date endTime;

}
