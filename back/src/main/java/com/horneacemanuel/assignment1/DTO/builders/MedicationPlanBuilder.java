package com.horneacemanuel.assignment1.DTO.builders;

import com.horneacemanuel.assignment1.DTO.MedicationPlanDTO;
import com.horneacemanuel.assignment1.entity.MedicationPerPlan;
import com.horneacemanuel.assignment1.entity.MedicationPlan;

public class MedicationPlanBuilder {

    public static MedicationPlanDTO generateDTOFromEntity(MedicationPlan medicationPlan){
        StringBuilder medications = new StringBuilder();

        for(MedicationPerPlan medicationPerPlan: medicationPlan.getMedicationsPerPlan()){
            medications.append(medicationPerPlan.getMedication().getName());
            medications.append("-" + medicationPerPlan.getDosage().toString());
            medications.append("-" + medicationPerPlan.getIntakeMoments());
        }

        return new MedicationPlanDTO(
                medicationPlan.getId(),
                medicationPlan.getPatient().getUsername(),
                medications.toString(),
                medicationPlan.getStartTime(),
                medicationPlan.getEndTime()
        );
    }
}
