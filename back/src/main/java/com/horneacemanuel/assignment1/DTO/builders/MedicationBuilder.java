package com.horneacemanuel.assignment1.DTO.builders;

import com.horneacemanuel.assignment1.DTO.MedicationDTO;
import com.horneacemanuel.assignment1.entity.Medication;
import com.horneacemanuel.assignment1.entity.MedicationPerPlan;

import java.util.ArrayList;
import java.util.List;

public class MedicationBuilder {
    public static MedicationDTO generateDTOFromEntity(Medication medication){
        return new MedicationDTO(
                medication.getId(),
                medication.getName(),
                medication.getSideEffects()
        );
    }

    public static Medication generateEntityFromDTO(MedicationDTO medicationDTO){
        return new Medication(
                medicationDTO.getId(),
                new ArrayList<MedicationPerPlan>(),
                medicationDTO.getName(),
                medicationDTO.getSideEffects()

        );
    }

    public static Medication generateEntityFromDTO(MedicationDTO medicationDTO, List<MedicationPerPlan> medicationPerPlans){
        return new Medication(
                medicationDTO.getId(),
                medicationPerPlans,
                medicationDTO.getName(),
                medicationDTO.getSideEffects()
        );
    }
}
