package com.horneacemanuel.assignment1.DTO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PatientViewDTO {
    private Integer id;
    private String name;
    private String doctor;
    private String caregiver;
    private String medicalRecord;
}
