package com.horneacemanuel.assignment1.DTO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MedicationDTO {
    private Integer id;
    private String name;
    private String sideEffects;
}
