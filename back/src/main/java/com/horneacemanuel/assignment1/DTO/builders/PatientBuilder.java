package com.horneacemanuel.assignment1.DTO.builders;

import com.horneacemanuel.assignment1.DTO.PatientDTO;
import com.horneacemanuel.assignment1.entity.Patient;

public class PatientBuilder {

    public static PatientDTO generateDTOFromEntity(Patient patient){
        return new PatientDTO(
                patient.getId(),
                UserBuilder.generateDTOFromEntity(patient.getUser()),
                patient.getDoctor().getId(),
                patient.getCaregiver().getId(),
                patient.getMedicalRecord()
        );
    }
}
