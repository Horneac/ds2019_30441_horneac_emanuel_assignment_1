package com.horneacemanuel.assignment1.DTO;

import com.horneacemanuel.assignment1.entity.Role;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserDTO {
    private Integer id;
    private String name;
    private String gender;
    private Date dateOfBirth;
    private String address;
    private String username;
    private String password;
    private Role role;

    public UserDTO(UserDTO userDTO){
        this.name = userDTO.getName();
        this.gender = userDTO.getGender();

    }
}
