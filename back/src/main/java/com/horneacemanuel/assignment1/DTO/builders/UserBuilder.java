package com.horneacemanuel.assignment1.DTO.builders;

import com.horneacemanuel.assignment1.DTO.UserDTO;
import com.horneacemanuel.assignment1.DTO.UserViewDTO;
import com.horneacemanuel.assignment1.entity.User;
import lombok.NoArgsConstructor;

@NoArgsConstructor
public class UserBuilder {

    public static UserDTO generateDTOFromEntity(User user){
        return new UserDTO(
                user.getId(),
                user.getName(),
                user.getGender(),
                user.getDateOfBirth(),
                user.getAddress(),
                user.getUsername(),
                user.getPassword(),
                user.getRole()
        );
    }

    public static User generateEntityFromDTO(UserDTO dto){
        return new User(
                dto.getId(),
                dto.getName(),
                dto.getGender(),
                dto.getDateOfBirth(),
                dto.getAddress(),
                dto.getName(),
                dto.getPassword(),
                dto.getRole()
        );
    }

    public static UserViewDTO generateViewDTOFromEntity(User user){
        return new UserViewDTO(
                user.getId(),
                user.getName(),
                user.getGender(),
                user.getDateOfBirth(),
                user.getAddress(),
                user.getUsername(),
                user.getRole()
        );
    }
}
