package com.horneacemanuel.assignment1.DTO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PatientDTO {
    private Integer id;
    private UserDTO user;
    private Integer doctorId;
    private Integer caregiverId;
    private String medicalRecord;
}
