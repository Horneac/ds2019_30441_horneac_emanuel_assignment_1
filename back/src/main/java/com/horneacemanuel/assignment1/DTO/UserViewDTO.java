package com.horneacemanuel.assignment1.DTO;

import com.horneacemanuel.assignment1.entity.Role;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserViewDTO {
    private Integer id;
    private String name;
    private String gender;
    private Date dateOfBirth;
    private String address;
    private String username;
    private Role role;
}
