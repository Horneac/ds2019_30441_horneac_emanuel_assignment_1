package com.horneacemanuel.assignment1.DTO.builders;

import com.horneacemanuel.assignment1.DTO.PatientViewDTO;
import com.horneacemanuel.assignment1.entity.Patient;

public class PatientViewBuilder {
    public static PatientViewDTO generateDTOFromEntity(Patient patient){
        return new PatientViewDTO(
                patient.getId(),
                patient.getUser().getName(),
                patient.getDoctor().getName(),
                patient.getCaregiver().getName(),
                patient.getMedicalRecord()
        );
    }
}
