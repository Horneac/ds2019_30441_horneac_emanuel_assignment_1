import { HOST } from "../../../commons/hosts";
import RestApiClient from "../../../commons/api/rest-client";

const endpoint = {
    get_caregivers: "/user/caregiver/",
    post_caregiver: "/user/caregiver/",
    delete_caregiver: "/user/caregiver/",
    put_caregiver: "/user/caregiver/"
};

function deleteCaregiver(id, callback) {
    let request = new Request(HOST.backend_api + endpoint.delete_caregiver + id, {
        method: "DELETE"
    });
    console.log("URL: " + request.url);
    RestApiClient.performRequest(request, callback);
}

function getCaregivers(callback) {
    let request = new Request(HOST.backend_api + endpoint.get_caregivers, {
        method: "GET"
    });
    console.log("URL: " + request.url);
    RestApiClient.performRequest(request, callback);
}

function getCaregiverById(params, callback) {
    let request = new Request(HOST.backend_api + endpoint.get_caregivers + params.id, {
        method: "GET"
    });

    console.log("URL: " + request.url);
    RestApiClient.performRequest(request, callback);
}
function putCaregiver(user, callback) {
    let request = new Request(HOST.backend_api + endpoint.post_caregiver, {
        method: "PUT",
        headers: {
            Accept: "application/json",
            "Content-Type": "application/json"
        },
        body: JSON.stringify(user)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}
function postCaregiver(user, callback) {
    let request = new Request(HOST.backend_api + endpoint.post_caregiver, {
        method: "POST",
        headers: {
            Accept: "application/json",
            "Content-Type": "application/json"
        },
        body: JSON.stringify(user)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

export { getCaregivers, getCaregiverById, putCaregiver, postCaregiver, deleteCaregiver };
