import React from "react";
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";
import { Card, Col, Row } from "reactstrap";
import Table from "../../commons/tables/table";
import CaregiverForm from "./caregiver-form";
import Button from "react-bootstrap/Button";
import * as API_CAREGIVERS from "./api/caregiver-api";

const onDelete = id => {
    console.log("this is the id:" + id);
};

function someCallback(result, status, err) {
    console.log(result);
    if (result !== null && status === 200) {
        console.log("Succesfully deleted caregiver.");
        window.location.href = "/caregivers";
    } else {
        console.log("Am prins o eroare!!!");
    }
}
const columns = [
    {
        Header: "Id",
        accessor: "id"
    },
    {
        Header: "Name",
        accessor: "name"
    },
    {
        Header: "Address",
        accessor: "address"
    },
    {
        Header: "Date of birth",
        accessor: "dateOfBirth"
    },
    {
        Header: "Gender",
        accessor: "gender"
    },
    {
        Header: "Delete caregiver",
        accessor: "deleteCaregiver",
        Cell: cellInfo => (
            <Button
                variant={"danger"}
                onClick={() => API_CAREGIVERS.deleteCaregiver(cellInfo.original.id, someCallback)}
            >
                Delete
            </Button>
        )
    }
];

const filters = [
    {
        accessor: "name"
    },
    {
        accessor: "address"
    }
];

class Caregivers extends React.Component {
    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.state = {
            collapseForm: true,
            loadPage: false,
            errorStatus: 0,
            error: null
        };

        this.tableData = [];
    }

    toggleForm() {
        this.setState({ collapseForm: !this.state.collapseForm });
    }

    componentDidMount() {
        this.fetchCaregivers();
    }

    fetchCaregivers() {
        return API_CAREGIVERS.getCaregivers((result, status, err) => {
            console.log(result);
            if (result !== null && status === 200) {
                result.forEach(x => {
                    this.tableData.push({
                        id: x.id,
                        name: x.name,
                        address: x.address,
                        dateOfBirth: x.dateOfBirth,
                        gender: x.gender
                    });
                });
                this.forceUpdate();
            } else {
                console.log("Am prins o eroare!!!");
                this.state.errorStatus = status;
                this.state.error = err;
                this.forceUpdate();
            }
        });
    }

    refresh() {
        this.forceUpdate();
    }

    render() {
        let pageSize = 5;
        return (
            <div>
                <Row>
                    <Col>
                        <Card body>
                            <Table data={this.tableData} columns={columns} search={filters} pageSize={pageSize} />
                        </Card>
                    </Col>
                </Row>

                <Row>
                    <Col>
                        <Card body>
                            <div>
                                <CaregiverForm registerCaregiver={this.refresh}></CaregiverForm>
                            </div>
                        </Card>
                    </Col>
                </Row>

                {this.state.errorStatus > 0 && (
                    <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error} />
                )}
            </div>
        );
    }
}

export default Caregivers;
