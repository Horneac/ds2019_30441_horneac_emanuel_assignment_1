import React from "react";
import validate from "./validators/caregiver-validators";
import TextInput from "./fields/TextInput";
import "./fields/fields.css";
import Button from "react-bootstrap/Button";
import * as API_USERS from "./api/caregiver-api";
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";

class CaregiverForm extends React.Component {
    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);

        this.state = {
            errorStatus: 0,
            error: null,

            formIsValid: false,

            formControls: {
                id: {
                    value: "",
                    placeholder: "the id of the caregiver you want to edit",
                    valid: true,
                    touched: false
                },
                name: {
                    value: "",
                    placeholder: "What is your name?...",
                    valid: false,
                    touched: false,
                    validationRules: {
                        minLength: 3,
                        isRequired: true
                    }
                },

                gender: {
                    value: "",
                    placeholder: "Gender",
                    valid: false,
                    touched: false,
                    validationRules: {
                        genderValidator: true
                    }
                },

                dateOfBirth: {
                    value: "",
                    placeholder: "yyyy-mm-dd",
                    valid: false,
                    touched: false
                },
                address: {
                    value: "",
                    placeholder: "Cluj, Zorilor, Str. Lalelelor 21...",
                    valid: false,
                    touched: false
                },
                username: {
                    value: "",
                    placeholder: "Please enter a username for the caregiver",
                    valid: false,
                    touched: false
                },
                password: {
                    value: "",
                    placeholder: "Please enter a password for the caregiver",
                    valid: false,
                    touched: false
                }
            }
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    toggleForm() {
        this.setState({ collapseForm: !this.state.collapseForm });
    }

    componentDidMount() {}

    handleChange = event => {
        const name = event.target.name;
        const value = event.target.value;

        const updatedControls = {
            ...this.state.formControls
        };

        const updatedFormElement = {
            ...updatedControls[name]
        };

        updatedFormElement.value = value;
        updatedFormElement.touched = true;
        updatedFormElement.valid = validate(value, updatedFormElement.validationRules);

        console.log("Element: " + name + " validated: " + updatedFormElement.valid);

        updatedControls[name] = updatedFormElement;

        let formIsValid = true;
        for (let updatedFormElementName in updatedControls) {
            formIsValid = updatedControls[updatedFormElementName].valid && formIsValid;
        }

        this.setState({
            formControls: updatedControls,
            formIsValid: formIsValid
        });
    };

    registerCaregiver(caregiver) {
        return API_USERS.postCaregiver(caregiver, (result, status, error) => {
            console.log(result);

            if (result !== null && (status === 200 || status === 201)) {
                console.log("Successfully inserted caregiver with id: " + result);
                this.props.refresh();
            } else {
                this.state.errorStatus = status;
                this.error = error;
            }
        });
    }

    updateCaregiver(caregiver) {
        return API_USERS.putCaregiver(caregiver, (result, status, error) => {
            console.log(result);

            if (result !== null && (status === 200 || status === 201)) {
                console.log("Successfully updated caregiver with id: " + result);
                window.location.href = "/caregivers";
            } else {
                this.state.errorStatus = status;
                this.error = error;
            }
        });
    }

    handleUpdate() {
        let caregiver = {
            id: this.state.formControls.id.value,
            name: this.state.formControls.name.value,
            gender: this.state.formControls.gender.value,
            dateOfBirth: this.state.formControls.dateOfBirth.value,
            address: this.state.formControls.address.value,
            username: this.state.formControls.username.value,
            password: this.state.formControls.password.value,
            role: "CAREGIVER"
        };

        this.updateCaregiver(caregiver);
    }

    handleSubmit() {
        console.log("New caregiver data:");
        console.log("Name: " + this.state.formControls.name.value);
        console.log("gender: " + this.state.formControls.gender.value);
        console.log("dateOfBirth: " + this.state.formControls.dateOfBirth.value);
        console.log("address: " + this.state.formControls.address.value);
        console.log("username: " + this.state.formControls.username.value);
        console.log("password: " + this.state.formControls.password.value);

        let caregiver = {
            id: 0,
            name: this.state.formControls.name.value,
            gender: this.state.formControls.gender.value,
            dateOfBirth: this.state.formControls.dateOfBirth.value,
            address: this.state.formControls.address.value,
            username: this.state.formControls.username.value,
            password: this.state.formControls.password.value,
            role: "CAREGIVER"
        };

        this.registerCaregiver(caregiver);
    }

    render() {
        return (
            <form onSubmit={this.handleSubmit}>
                <h1>Update/Insert new caregiver</h1>
                <p> Id: </p>

                <TextInput
                    name="id"
                    placeholder={this.state.formControls.id.placeholder}
                    value={this.state.formControls.id.value}
                    onChange={this.handleChange}
                    touched={this.state.formControls.id.touched}
                    valid={this.state.formControls.id.valid}
                />
                <p> Name: </p>

                <TextInput
                    name="name"
                    placeholder={this.state.formControls.name.placeholder}
                    value={this.state.formControls.name.value}
                    onChange={this.handleChange}
                    touched={this.state.formControls.name.touched}
                    valid={this.state.formControls.name.valid}
                />
                {this.state.formControls.name.touched && !this.state.formControls.name.valid && (
                    <div className={"error-message row"}> * Name must have at least 3 characters </div>
                )}

                <p> Gender: </p>
                <TextInput
                    name="gender"
                    placeholder={this.state.formControls.gender.placeholder}
                    value={this.state.formControls.gender.value}
                    onChange={this.handleChange}
                    touched={this.state.formControls.gender.touched}
                    valid={this.state.formControls.gender.valid}
                />
                {this.state.formControls.gender.touched && !this.state.formControls.gender.valid && (
                    <div className={"error-message"}> * gender must have a valid format</div>
                )}

                <p> Date of birth: </p>
                <TextInput
                    name="dateOfBirth"
                    placeholder={this.state.formControls.dateOfBirth.placeholder}
                    value={this.state.formControls.dateOfBirth.value}
                    onChange={this.handleChange}
                    touched={this.state.formControls.dateOfBirth.touched}
                    valid={this.state.formControls.dateOfBirth.valid}
                />

                <p> Address: </p>
                <TextInput
                    name="address"
                    placeholder={this.state.formControls.address.placeholder}
                    value={this.state.formControls.address.value}
                    onChange={this.handleChange}
                    touched={this.state.formControls.address.touched}
                    valid={this.state.formControls.address.valid}
                />

                <p> Username: </p>
                <TextInput
                    name="username"
                    placeholder={this.state.formControls.username.placeholder}
                    value={this.state.formControls.username.value}
                    onChange={this.handleChange}
                    touched={this.state.formControls.username.touched}
                    valid={this.state.formControls.username.valid}
                />

                <p> password: </p>
                <TextInput
                    name="password"
                    placeholder={this.state.formControls.password.placeholder}
                    value={this.state.formControls.password.value}
                    onChange={this.handleChange}
                    touched={this.state.formControls.password.touched}
                    valid={this.state.formControls.password.valid}
                />

                <p></p>
                <Button variant="success" type={"submit"} disabled={!this.state.formIsValid}>
                    Submit
                </Button>
                <Button
                    variant="warning"
                    onClick={() => this.handleUpdate()}
                    disabled={!(this.state.formIsValid && this.state.formControls.id.value.length > 0)}
                >
                    Update
                </Button>

                {this.state.errorStatus > 0 && (
                    <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error} />
                )}
            </form>
        );
    }
}

export default CaregiverForm;
