import React from "react";
import styles from "../styles/project-style.css";

class UnauthorizedPage extends React.Component {
    render() {
        return <h3 className={styles.errorTitle}>You do not have permissions to access this page</h3>;
    }
}

export default UnauthorizedPage;
