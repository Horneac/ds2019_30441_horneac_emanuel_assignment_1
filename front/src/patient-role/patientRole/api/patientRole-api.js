import { HOST } from "../../../commons/hosts";
import RestApiClient from "../../../commons/api/rest-client";

const endpoint = {
    get_patient: "/patient/info/",
    get_medications: "/medicationPlan/"
};

function getPatient(callback) {
    let request = new Request(HOST.backend_api + endpoint.get_patient + localStorage.getItem("username"), {
        method: "GET"
    });
    console.log("URL: " + request.url);
    RestApiClient.performRequest(request, callback);
}

function getPatientMedications(callback) {
    let request = new Request(HOST.backend_api + endpoint.get_medications + localStorage.getItem("username"), {
        method: "GET"
    });
    console.log("URL: " + request.url);
    RestApiClient.performRequest(request, callback);
}

export { getPatient, getPatientMedications };
