import React from "react";
import "./fields/fields.css";
import * as API_USERS from "./api/patientRole-api";

class PatientRoleForm extends React.Component {
    constructor(props) {
        super(props);
        this.error = 0;
        this.state = {
            id: 0,
            name: "",
            doctor: "",
            caregiver: "",
            medicalRecord: ""
        };

        API_USERS.getPatient((result, status, error) => {
            console.log(result);

            if (result !== null && (status === 200 || status === 201)) {
                this.setState({
                    id: result.id,
                    name: result.name,
                    doctor: result.doctor,
                    caregiver: result.caregiver,
                    medicalRecord: result.medicalRecord
                });
            } else {
                this.state.errorStatus = status;
                this.error = error;
            }
        });
    }

    componentDidMount() {}

    render() {
        return (
            <form onSubmit={this.handleSubmit}>
                <h1></h1>
                <p> Id: {this.state.id}</p>

                <p></p>
                <p> Name: {this.state.name} </p>

                <p></p>
                <p> doctor: {this.state.doctor}</p>
                <p></p>

                <p> caregiver: {this.state.caregiver}</p>
                <p></p>

                <p>medical record: {this.state.medicalRecord}</p>
                <p></p>
            </form>
        );
    }
}

export default PatientRoleForm;
