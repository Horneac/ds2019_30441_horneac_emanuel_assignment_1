import React from "react";
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";
import { Card, Col, Row } from "reactstrap";
import Table from "../../commons/tables/table";
import PatientRoleForm from "./patientRole-form";
import Button from "react-bootstrap/Button";
import * as API_PATIENTS from "./api/patientRole-api";

const columns = [
    {
        Header: "Id",
        accessor: "id"
    },
    {
        Header: "Medications",
        accessor: "medications"
    },
    {
        Header: "start time",
        accessor: "startTime"
    },
    {
        Header: "end time",
        accessor: "endTime"
    }
];

const filters = [
    {
        accessor: "medications"
    }
];

class PatientRoles extends React.Component {
    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.state = {
            collapseForm: true,
            loadPage: false,
            errorStatus: 0,
            error: null
        };

        this.tableData = [];
    }

    toggleForm() {
        this.setState({ collapseForm: !this.state.collapseForm });
    }

    componentDidMount() {
        this.fetchPatientRoles();
    }

    fetchPatientRoles() {
        return API_PATIENTS.getPatientMedications((result, status, err) => {
            console.log(result);
            if (result !== null && status === 200) {
                result.forEach(x => {
                    this.tableData.push({
                        id: x.id,
                        medications: x.medications,
                        startTime: x.startTime,
                        endTime: x.endTime
                    });
                });
                this.forceUpdate();
            } else {
                console.log("Am prins o eroare!!!");
                this.state.errorStatus = status;
                this.state.error = err;
                this.forceUpdate();
            }
        });
    }

    refresh() {
        this.forceUpdate();
    }

    render() {
        let pageSize = 5;
        return (
            <div>
                <Row>
                    <Col>
                        <Card body>
                            <Table data={this.tableData} columns={columns} search={filters} pageSize={pageSize} />
                        </Card>
                    </Col>
                </Row>

                <Row>
                    <Col>
                        <Card body>
                            <div>
                                <PatientRoleForm registerPatientRole={this.refresh}></PatientRoleForm>
                            </div>
                        </Card>
                    </Col>
                </Row>

                {this.state.errorStatus > 0 && (
                    <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error} />
                )}
            </div>
        );
    }
}

export default PatientRoles;
