import { HOST } from "../../../commons/hosts";
import RestApiClient from "../../../commons/api/rest-client";

const endpoint = {
    get_patients: "/patient/",
    post_patient: "/patient/"
};

function deletePatient(id, callback) {
    let request = new Request(HOST.backend_api + endpoint.post_patient + id, {
        method: "DELETE"
    });
    console.log("URL: " + request.url);
    RestApiClient.performRequest(request, callback);
}

function getPatients(callback) {
    let request = new Request(HOST.backend_api + endpoint.get_patients, {
        method: "GET"
    });
    console.log("URL: " + request.url);
    RestApiClient.performRequest(request, callback);
}

function getPatientById(params, callback) {
    let request = new Request(HOST.backend_api + endpoint.get_patients + params.id, {
        method: "GET"
    });

    console.log("URL: " + request.url);
    RestApiClient.performRequest(request, callback);
}
function putPatient(user, callback) {
    let request = new Request(HOST.backend_api + endpoint.post_patient, {
        method: "PUT",
        headers: {
            Accept: "application/json",
            "Content-Type": "application/json"
        },
        body: JSON.stringify(user)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}
function postPatient(user, callback) {
    let request = new Request(HOST.backend_api + endpoint.post_patient, {
        method: "POST",
        headers: {
            Accept: "application/json",
            "Content-Type": "application/json"
        },
        body: JSON.stringify(user)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

export { getPatients, getPatientById, putPatient, postPatient, deletePatient };
