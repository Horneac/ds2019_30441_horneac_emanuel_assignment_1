import { HOST } from "../../commons/hosts";
import RestApiClient from "../../commons/api/rest-client";

function login(user, callback) {
    let request = new Request(HOST.backend_api + "/user/login", {
        method: "PUT",
        headers: {
            Accept: "application/json",
            Authorization: "Basic " + btoa(user.username + ":" + user.password),
            "Content-Type": "application/json"
        },
        body: JSON.stringify(user)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

export default login;
