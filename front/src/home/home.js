import React from "react";

import BackgroundImg from "../commons/images/future-medicine.jpg";

import { Button, Container, Jumbotron } from "reactstrap";

import { useCookies } from "react-cookie";
import TextInput from "../medication-data/medication/fields/TextInput";
import login from "./api/login-api";

const backgroundStyle = {
    backgroundPosition: "center",
    backgroundSize: "cover",
    backgroundRepeat: "no-repeat",
    width: "100%",
    height: "1920px",
    backgroundImage: `url(${BackgroundImg})`
};
const textStyle = { color: "white" };

class Home extends React.Component {
    constructor(props) {
        super(props);
        localStorage.setItem("role", "NONE");
        this.state = {
            role: "",
            formControls: {
                username: {
                    value: "",
                    placeholder: ""
                },
                password: {
                    value: "",
                    placeholder: ""
                }
            }
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleLogin = this.handleLogin.bind(this);
    }

    handleChange = event => {
        const name = event.target.name;
        const value = event.target.value;

        const updatedControls = {
            ...this.state.formControls
        };

        const updatedFormElement = {
            ...updatedControls[name]
        };

        updatedFormElement.value = value;

        updatedControls[name] = updatedFormElement;
        console.log("Element: " + name);

        this.setState({
            formControls: updatedControls
        });
    };

    handleLogin() {
        let user = {
            username: this.state.formControls.username.value,
            password: this.state.formControls.password.value
        };
        login(user, (result, status, error) => {
            console.log(result);

            if (result !== null && (status === 200 || status === 201)) {
                console.log("Successfully logged in as: " + result);
                localStorage.setItem("role", result);
                localStorage.setItem("username", user.username);
                switch (result) {
                    case "DOCTOR":
                        window.location.href = "/patients";
                        break;
                    case "PATIENT":
                        window.location.href = "/patientPage";
                        break;
                    case "CAREGIVER":
                        window.location.href = "/caregiver";
                        break;

                    default:
                        break;
                }
            } else {
                this.state.errorStatus = status;
                this.error = error;
            }
        });
    }

    render() {
        return (
            <div>
                <Jumbotron fluid style={backgroundStyle}>
                    <Container fluid>
                        <h1 className="display-3" style={textStyle}>
                            Integrated Medical Monitoring Platform for Home-care assistance
                        </h1>
                        <p className="lead" style={textStyle}>
                            {" "}
                            <b>
                                Enabling real time monitoring of patients, remote-assisted care services and smart
                                intake mechanism for prescribed medication.
                            </b>{" "}
                        </p>
                        <hr className="my-2" />
                        <p style={textStyle}>
                            {" "}
                            <b>
                                This assignment represents the first module of the distributed software system
                                "Integrated Medical Monitoring Platform for Home-care assistance that represents the
                                final project for the Distributed Systems course.{" "}
                            </b>{" "}
                        </p>

                        <TextInput
                            name="username"
                            onChange={this.handleChange}
                            value={this.state.formControls.username.value}
                        ></TextInput>

                        <TextInput
                            name="password"
                            onChange={this.handleChange}
                            value={this.state.formControls.password.value}
                        ></TextInput>
                        <Button variant="primary" onClick={this.handleLogin}>
                            Login
                        </Button>
                        <p></p>
                        <p className="lead">
                            <Button
                                color="primary"
                                onClick={() => window.open("http://coned.utcluj.ro/~salomie/DS_Lic/")}
                            >
                                Learn More
                            </Button>
                        </p>
                    </Container>
                </Jumbotron>
            </div>
        );
    }
}

export default Home;
