import React from "react";
import validate from "./validators/medication-validators";
import TextInput from "./fields/TextInput";
import "./fields/fields.css";
import Button from "react-bootstrap/Button";
import * as API_USERS from "./api/medication-api";
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";

class MedicationForm extends React.Component {
    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);

        this.state = {
            errorStatus: 0,
            error: null,

            formIsValid: false,

            formControls: {
                id: {
                    value: "",
                    placeholder: "the id of the medication you want to edit",
                    valid: true,
                    touched: false
                },
                name: {
                    value: "",
                    placeholder: "What is the name of the medication?...",
                    valid: false,
                    touched: false,
                    validationRules: {
                        minLength: 3,
                        isRequired: true
                    }
                },
                sideEffects: {
                    value: "",
                    placeholder: "What are the side effects of the medication?...",
                    valid: false,
                    touched: false,
                    validationRules: {
                        minLength: 3,
                        isRequired: true
                    }
                }
            }
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    toggleForm() {
        this.setState({ collapseForm: !this.state.collapseForm });
    }

    componentDidMount() {}

    handleChange = event => {
        const name = event.target.name;
        const value = event.target.value;

        const updatedControls = {
            ...this.state.formControls
        };

        const updatedFormElement = {
            ...updatedControls[name]
        };

        updatedFormElement.value = value;
        updatedFormElement.touched = true;
        updatedFormElement.valid = validate(value, updatedFormElement.validationRules);

        console.log("Element: " + name + " validated: " + updatedFormElement.valid);

        updatedControls[name] = updatedFormElement;

        let formIsValid = true;
        for (let updatedFormElementName in updatedControls) {
            formIsValid = updatedControls[updatedFormElementName].valid && formIsValid;
        }

        this.setState({
            formControls: updatedControls,
            formIsValid: formIsValid
        });
    };

    registerMedication(medication) {
        return API_USERS.postMedication(medication, (result, status, error) => {
            console.log(result);

            if (result !== null && (status === 200 || status === 201)) {
                console.log("Successfully inserted medication with id: " + result);
                this.props.refresh();
            } else {
                this.state.errorStatus = status;
                this.error = error;
            }
        });
    }

    updateMedication(medication) {
        return API_USERS.putMedication(medication, (result, status, error) => {
            console.log(result);

            if (result !== null && (status === 200 || status === 201)) {
                console.log("Successfully updated medication with id: " + result);
                window.location.href = "/medications";
            } else {
                this.state.errorStatus = status;
                this.error = error;
            }
        });
    }

    handleUpdate() {
        let medication = {
            id: this.state.formControls.id.value,
            name: this.state.formControls.name.value,
            sideEffects: this.state.formControls.sideEffects.value
        };
        this.updateMedication(medication);
    }

    handleSubmit() {
        console.log("New medication data:");
        console.log("Name: " + this.state.formControls.name.value);
        console.log("sideEffects: " + this.state.formControls.sideEffects.value);

        let medication = {
            id: 0,
            name: this.state.formControls.name.value,
            sideEffects: this.state.formControls.sideEffects.value
        };

        this.registerMedication(medication);
    }

    render() {
        return (
            <form onSubmit={this.handleSubmit}>
                <h1>Update/Insert new medication</h1>
                <p> Id: </p>

                <TextInput
                    name="id"
                    placeholder={this.state.formControls.id.placeholder}
                    value={this.state.formControls.id.value}
                    onChange={this.handleChange}
                    touched={this.state.formControls.id.touched}
                    valid={this.state.formControls.id.valid}
                />
                <p> Name: </p>

                <TextInput
                    name="name"
                    placeholder={this.state.formControls.name.placeholder}
                    value={this.state.formControls.name.value}
                    onChange={this.handleChange}
                    touched={this.state.formControls.name.touched}
                    valid={this.state.formControls.name.valid}
                />
                {this.state.formControls.name.touched && !this.state.formControls.name.valid && (
                    <div className={"error-message row"}> * Name must have at least 3 characters </div>
                )}

                <p> Side Effects: </p>
                <TextInput
                    name="sideEffects"
                    placeholder={this.state.formControls.sideEffects.placeholder}
                    value={this.state.formControls.sideEffects.value}
                    onChange={this.handleChange}
                    touched={this.state.formControls.sideEffects.touched}
                    valid={this.state.formControls.sideEffects.valid}
                />

                <p></p>
                <Button variant="success" type={"submit"} disabled={!this.state.formIsValid}>
                    Submit
                </Button>
                <Button
                    variant="warning"
                    onClick={() => this.handleUpdate()}
                    disabled={!(this.state.formIsValid && this.state.formControls.id.value.length > 0)}
                >
                    Update
                </Button>

                {this.state.errorStatus > 0 && (
                    <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error} />
                )}
            </form>
        );
    }
}

export default MedicationForm;
