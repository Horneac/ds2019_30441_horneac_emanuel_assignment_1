import { HOST } from "../../../commons/hosts";
import RestApiClient from "../../../commons/api/rest-client";

const endpoint = {
    get_medications: "/medication/",
    post_medication: "/medication/",
    delete_medications: "/medication/",
    put_medication: "/medication/"
};

function deleteMedication(id, callback) {
    let request = new Request(HOST.backend_api + endpoint.post_medication + id, {
        method: "DELETE"
    });
    console.log("URL: " + request.url);
    RestApiClient.performRequest(request, callback);
}

function getMedications(callback) {
    let request = new Request(HOST.backend_api + endpoint.get_medications, {
        method: "GET"
    });
    console.log("URL: " + request.url);
    RestApiClient.performRequest(request, callback);
}

function getMedicationById(params, callback) {
    let request = new Request(HOST.backend_api + endpoint.get_medications + params.id, {
        method: "GET"
    });

    console.log("URL: " + request.url);
    RestApiClient.performRequest(request, callback);
}
function putMedication(user, callback) {
    let request = new Request(HOST.backend_api + endpoint.post_medication, {
        method: "PUT",
        headers: {
            Accept: "application/json",
            "Content-Type": "application/json"
        },
        body: JSON.stringify(user)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}
function postMedication(user, callback) {
    let request = new Request(HOST.backend_api + endpoint.post_medication, {
        method: "POST",
        headers: {
            Accept: "application/json",
            "Content-Type": "application/json"
        },
        body: JSON.stringify(user)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

export { getMedications, getMedicationById, putMedication, postMedication, deleteMedication };
