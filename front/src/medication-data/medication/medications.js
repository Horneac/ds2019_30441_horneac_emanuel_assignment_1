import React from "react";
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";
import { Card, Col, Row } from "reactstrap";
import Table from "../../commons/tables/table";
import MedicationForm from "./medication-form";
import Button from "react-bootstrap/Button";
import * as API_PATIENTS from "./api/medication-api";

function someCallback(result, status, err) {
    console.log(result);
    if (result !== null && status === 200) {
        console.log("Succesfully deleted medication.");
        window.location.href = "/medications";
    } else {
        console.log("Am prins o eroare!!!");
    }
}
const columns = [
    {
        Header: "Id",
        accessor: "id"
    },
    {
        Header: "Name",
        accessor: "name"
    },
    {
        Header: "Side Effects",
        accessor: "sideEffects"
    },
    {
        Header: "Delete medication",
        accessor: "deleteMedication",
        Cell: cellInfo => (
            <Button
                variant={"danger"}
                onClick={() => API_PATIENTS.deleteMedication(cellInfo.original.id, someCallback)}
            >
                Delete
            </Button>
        )
    }
];

const filters = [
    {
        accessor: "name"
    }
];

class Medications extends React.Component {
    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.state = {
            collapseForm: true,
            loadPage: false,
            errorStatus: 0,
            error: null
        };

        this.tableData = [];
    }

    toggleForm() {
        this.setState({ collapseForm: !this.state.collapseForm });
    }

    componentDidMount() {
        this.fetchMedications();
    }

    fetchMedications() {
        return API_PATIENTS.getMedications((result, status, err) => {
            console.log(result);
            if (result !== null && status === 200) {
                result.forEach(x => {
                    this.tableData.push({
                        id: x.id,
                        name: x.name,
                        sideEffects: x.sideEffects
                    });
                });
                this.forceUpdate();
            } else {
                console.log("Am prins o eroare!!!");
                this.state.errorStatus = status;
                this.state.error = err;
                this.forceUpdate();
            }
        });
    }

    refresh() {
        this.forceUpdate();
    }

    render() {
        let pageSize = 5;
        return (
            <div>
                <Row>
                    <Col>
                        <Card body>
                            <Table data={this.tableData} columns={columns} search={filters} pageSize={pageSize} />
                        </Card>
                    </Col>
                </Row>

                <Row>
                    <Col>
                        <Card body>
                            <div>
                                <MedicationForm registerMedication={this.refresh}></MedicationForm>
                            </div>
                        </Card>
                    </Col>
                </Row>

                {this.state.errorStatus > 0 && (
                    <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error} />
                )}
            </div>
        );
    }
}

export default Medications;
