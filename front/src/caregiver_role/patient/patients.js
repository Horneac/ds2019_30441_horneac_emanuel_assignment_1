import React from "react";
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";
import { Card, Col, Row } from "reactstrap";
import Table from "../../commons/tables/table";
import * as API_PATIENTS from "./api/patient-api";

const columns = [
    {
        Header: "Id",
        accessor: "id"
    },
    {
        Header: "Name",
        accessor: "name"
    },
    {
        Header: "MedicalRecord",
        accessor: "medicalRecord"
    },
    {
        Header: "Doctor name",
        accessor: "doctor"
    },
    {
        Header: "Caregiver name",
        accessor: "caregiver"
    }
];

const filters = [
    {
        accessor: "name"
    },
    {
        accessor: "doctor"
    }
];

class CaregiverPage extends React.Component {
    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.state = {
            collapseForm: true,
            loadPage: false,
            errorStatus: 0,
            error: null
        };

        this.tableData = [];
    }

    toggleForm() {
        this.setState({ collapseForm: !this.state.collapseForm });
    }

    componentDidMount() {
        this.fetchPatients();
    }

    fetchPatients() {
        return API_PATIENTS.getPatients((result, status, err) => {
            console.log(result);
            if (result !== null && status === 200) {
                result.forEach(x => {
                    this.tableData.push({
                        id: x.id,
                        name: x.name,
                        medicalRecord: x.medicalRecord,
                        doctor: x.doctor,
                        caregiver: x.caregiver
                    });
                });
                this.forceUpdate();
            } else {
                console.log("Am prins o eroare!!!");
                this.state.errorStatus = status;
                this.state.error = err;
                this.forceUpdate();
            }
        });
    }

    refresh() {
        this.forceUpdate();
    }

    render() {
        let pageSize = 5;
        return (
            <div>
                <Row>
                    <Col>
                        <Card body>
                            <Table data={this.tableData} columns={columns} search={filters} pageSize={pageSize} />
                        </Card>
                    </Col>
                </Row>

                {this.state.errorStatus > 0 && (
                    <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error} />
                )}
            </div>
        );
    }
}

export default CaregiverPage;
