import React from "react";
import validate from "./validators/medicationPlan-validators";
import TextInput from "./fields/TextInput";
import "./fields/fields.css";
import Button from "react-bootstrap/Button";
import * as API_USERS from "./api/medicationPlan-api";
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";

class MedicationPlanForm extends React.Component {
    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);

        this.state = {
            errorStatus: 0,
            error: null,

            formIsValid: false,

            formControls: {
                id: {
                    value: "",
                    placeholder: "the id of the medicationPlan you want to edit",
                    valid: true,
                    touched: false
                },
                username: {
                    value: "",
                    placeholder: "patients name",
                    valid: false,
                    touched: false
                },

                medications: {
                    value: "",
                    placeholder: "ampicilina-0.5-MORNING-EVENING ...",
                    valid: false,
                    touched: false
                },

                startTime: {
                    value: "",
                    placeholder: "yyyy-mm-dd",
                    valid: false,
                    touched: false
                },
                endTime: {
                    value: "",
                    placeholder: "yyy-mm-dd",
                    valid: false,
                    touched: false
                }
            }
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    toggleForm() {
        this.setState({ collapseForm: !this.state.collapseForm });
    }

    componentDidMount() {}

    handleChange = event => {
        const name = event.target.name;
        const value = event.target.value;

        const updatedControls = {
            ...this.state.formControls
        };

        const updatedFormElement = {
            ...updatedControls[name]
        };

        updatedFormElement.value = value;
        updatedFormElement.touched = true;
        updatedFormElement.valid = validate(value, updatedFormElement.validationRules);

        console.log("Element: " + name + " validated: " + updatedFormElement.valid);

        updatedControls[name] = updatedFormElement;

        let formIsValid = true;
        for (let updatedFormElementName in updatedControls) {
            formIsValid = updatedControls[updatedFormElementName].valid && formIsValid;
        }

        this.setState({
            formControls: updatedControls,
            formIsValid: formIsValid
        });
    };

    registerMedicationPlan(medicationPlan) {
        return API_USERS.postMedicationPlan(medicationPlan, (result, status, error) => {
            console.log(result);

            if (result !== null && (status === 200 || status === 201)) {
                console.log("Successfully inserted medicationPlan with id: " + result);
                //this.props.refresh();
            } else {
                this.state.errorStatus = status;
                this.error = error;
            }
        });
    }

    handleUpdate() {
        let medicationPlan = {
            id: 0,
            username: this.state.formControls.username.value,
            medications: this.state.formControls.medications.value,
            startTime: this.state.formControls.startTime.value,
            endTime: this.state.formControls.endTime.value
        };

        this.updateMedicationPlan(medicationPlan);
    }

    handleSubmit() {
        console.log("New medicationPlan data:");
        console.log("user Name: " + this.state.formControls.username.value);
        console.log("medications: " + this.state.formControls.medications.value);
        console.log("start time: " + this.state.formControls.startTime.value);
        console.log("end time: " + this.state.formControls.endTime.value);

        let medicationPlan = {
            id: 0,
            username: this.state.formControls.username.value,
            medications: this.state.formControls.medications.value,
            startTime: this.state.formControls.startTime.value,
            endTime: this.state.formControls.endTime.value
        };

        this.registerMedicationPlan(medicationPlan);
    }

    render() {
        return (
            <form onSubmit={this.handleSubmit}>
                <h1>Insert new medicationPlan</h1>
                <p> Id: </p>

                <TextInput
                    name="id"
                    placeholder={this.state.formControls.id.placeholder}
                    value={this.state.formControls.id.value}
                    onChange={this.handleChange}
                    touched={this.state.formControls.id.touched}
                    valid={this.state.formControls.id.valid}
                />
                <p> user Name: </p>

                <TextInput
                    name="username"
                    placeholder={this.state.formControls.username.placeholder}
                    value={this.state.formControls.username.value}
                    onChange={this.handleChange}
                    touched={this.state.formControls.username.touched}
                    valid={this.state.formControls.username.valid}
                />
                {this.state.formControls.username.touched && !this.state.formControls.username.valid && (
                    <div className={"error-message row"}> * Name must have at least 3 characters </div>
                )}

                <p> Medications: </p>
                <TextInput
                    name="medications"
                    placeholder={this.state.formControls.medications.placeholder}
                    value={this.state.formControls.medications.value}
                    onChange={this.handleChange}
                    touched={this.state.formControls.medications.touched}
                    valid={this.state.formControls.medications.valid}
                />

                <p> start time </p>
                <TextInput
                    name="startTime"
                    placeholder={this.state.formControls.startTime.placeholder}
                    value={this.state.formControls.startTime.value}
                    onChange={this.handleChange}
                    touched={this.state.formControls.startTime.touched}
                    valid={this.state.formControls.startTime.valid}
                />

                <p> End time: </p>
                <TextInput
                    name="endTime"
                    placeholder={this.state.formControls.endTime.placeholder}
                    value={this.state.formControls.endTime.value}
                    onChange={this.handleChange}
                    touched={this.state.formControls.endTime.touched}
                    valid={this.state.formControls.endTime.valid}
                />

                <p></p>
                <Button variant="success" type={"submit"} disabled={!this.state.formIsValid}>
                    Submit
                </Button>

                {this.state.errorStatus > 0 && (
                    <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error} />
                )}
            </form>
        );
    }
}

export default MedicationPlanForm;
