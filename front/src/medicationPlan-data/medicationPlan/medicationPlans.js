import React from "react";
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";
import { Card, Col, Row } from "reactstrap";
import Table from "../../commons/tables/table";
import MedicationPlanForm from "./medicationPlan-form";
import Button from "react-bootstrap/Button";
import * as API_PATIENTS from "./api/medicationPlan-api";

function someCallback(result, status, err) {
    console.log(result);
    if (result !== null && status === 200) {
        console.log("Succesfully deleted medicationPlan.");
        window.location.href = "/medicationPlans";
    } else {
        console.log("Am prins o eroare!!!");
    }
}
const columns = [
    {
        Header: "Id",
        accessor: "id"
    },
    {
        Header: "userName",
        accessor: "username"
    },
    {
        Header: "Medications",
        accessor: "medications"
    },
    {
        Header: "start time",
        accessor: "startTime"
    },
    {
        Header: "end time",
        accessor: "endTime"
    }
];

const filters = [
    {
        accessor: "name"
    },
    {
        accessor: "medications"
    }
];

class MedicationPlans extends React.Component {
    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.state = {
            collapseForm: true,
            loadPage: false,
            errorStatus: 0,
            error: null
        };

        this.tableData = [];
    }

    toggleForm() {
        this.setState({ collapseForm: !this.state.collapseForm });
    }

    componentDidMount() {
        this.fetchMedicationPlans();
    }

    fetchMedicationPlans() {
        return API_PATIENTS.getMedicationPlans((result, status, err) => {
            console.log(result);
            if (result !== null && status === 200) {
                result.forEach(x => {
                    this.tableData.push({
                        id: x.id,
                        username: x.username,
                        medications: x.medications,
                        startTime: x.startTime,
                        endTime: x.endTime
                    });
                });
                this.forceUpdate();
            } else {
                console.log("Am prins o eroare!!!");
                this.state.errorStatus = status;
                this.state.error = err;
                this.forceUpdate();
            }
        });
    }

    refresh() {
        this.forceUpdate();
    }

    render() {
        let pageSize = 5;
        return (
            <div>
                <Row>
                    <Col>
                        <Card body>
                            <Table data={this.tableData} columns={columns} search={filters} pageSize={pageSize} />
                        </Card>
                    </Col>
                </Row>

                <Row>
                    <Col>
                        <Card body>
                            <div>
                                <MedicationPlanForm registerMedicationPlan={this.refresh}></MedicationPlanForm>
                            </div>
                        </Card>
                    </Col>
                </Row>

                {this.state.errorStatus > 0 && (
                    <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error} />
                )}
            </div>
        );
    }
}

export default MedicationPlans;
