import { HOST } from "../../../commons/hosts";
import RestApiClient from "../../../commons/api/rest-client";

const endpoint = {
    get_medicationPlans: "/medicationPlan/",
    post_medicationPlan: "/medicationPlan/"
};

function deleteMedicationPlan(id, callback) {
    let request = new Request(HOST.backend_api + endpoint.post_medicationPlan + id, {
        method: "DELETE"
    });
    console.log("URL: " + request.url);
    RestApiClient.performRequest(request, callback);
}

function getMedicationPlans(callback) {
    let request = new Request(HOST.backend_api + endpoint.get_medicationPlans, {
        method: "GET"
    });
    console.log("URL: " + request.url);
    RestApiClient.performRequest(request, callback);
}

function getMedicationPlanById(params, callback) {
    let request = new Request(HOST.backend_api + endpoint.get_medicationPlans + params.id, {
        method: "GET"
    });

    console.log("URL: " + request.url);
    RestApiClient.performRequest(request, callback);
}
function putMedicationPlan(user, callback) {
    let request = new Request(HOST.backend_api + endpoint.post_medicationPlan, {
        method: "PUT",
        headers: {
            Accept: "application/json",
            "Content-Type": "application/json"
        },
        body: JSON.stringify(user)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}
function postMedicationPlan(user, callback) {
    let request = new Request(HOST.backend_api + endpoint.post_medicationPlan, {
        method: "POST",
        headers: {
            Accept: "application/json",
            "Content-Type": "application/json"
        },
        body: JSON.stringify(user)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

export { getMedicationPlans, getMedicationPlanById, putMedicationPlan, postMedicationPlan, deleteMedicationPlan };
