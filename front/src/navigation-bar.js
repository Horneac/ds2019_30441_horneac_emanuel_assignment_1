import React from "react";
import logo from "./commons/images/icon.png";

import {
    DropdownItem,
    DropdownMenu,
    DropdownToggle,
    Nav,
    Navbar,
    NavbarBrand,
    NavLink,
    UncontrolledDropdown
} from "reactstrap";

const textStyle = {
    color: "white",
    textDecoration: "none"
};

const NavigationBar = ({ role }) => (
    <div>
        <Navbar color="dark" light expand="md">
            <NavbarBrand href="/">
                <img src={logo} width={"50"} height={"35"} />
            </NavbarBrand>
            <Nav className="mr-auto" navbar>
                <UncontrolledDropdown nav inNavbar>
                    <DropdownToggle style={textStyle} nav caret>
                        Menu
                    </DropdownToggle>
                    <DropdownMenu right>
                        <DropdownItem>
                            <NavLink href="/caregiver">Caregiver page</NavLink>
                            <NavLink href="/patients">Patients</NavLink>
                            <NavLink href="/caregivers">Caregivers</NavLink>
                            <NavLink href="/medications">Medications</NavLink>
                            <NavLink href="/medicationPlan">Medication Plans</NavLink>
                            <NavLink href="/patientPage">Patient page</NavLink>
                        </DropdownItem>
                    </DropdownMenu>
                </UncontrolledDropdown>
            </Nav>
            <p> Logged in as {role}</p>
        </Navbar>
    </div>
);

export default NavigationBar;
