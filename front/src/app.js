import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import NavigationBar from "./navigation-bar";
import Home from "./home/home";
import Patients from "./patient-data/patient/patients";
import Caregivers from "./caregivers-data/caregiver/caregivers";
import Medications from "./medication-data/medication/medications";
import CaregiverPage from "./caregiver_role/patient/patients";
import ErrorPage from "./commons/errorhandling/error-page";
import UnauthorizedPage from "./commons/errorhandling/not-allowed-page";
import styles from "./commons/styles/project-style.css";
import { CookiesProvider } from "react-cookie";
import MedicationPlans from "./medicationPlan-data/medicationPlan/medicationPlans";
import PatientRoles from "./patient-role/patientRole/patientsRole";

let enums = require("./commons/constants/enums");

class App extends React.Component {
    render() {
        return (
            <CookiesProvider>
                <div className={styles.back}>
                    <Router>
                        <div>
                            <NavigationBar role={localStorage.getItem("role")} />
                            <Switch>
                                <Route exact path="/" render={() => <Home />} />
                                <Route
                                    exact
                                    path="/patients"
                                    render={() => {
                                        if (localStorage.getItem("role") === "DOCTOR") {
                                            return <Patients />;
                                        } else {
                                            return <UnauthorizedPage />;
                                        }
                                    }}
                                />

                                <Route
                                    exact
                                    path="/caregivers"
                                    render={() => {
                                        if (localStorage.getItem("role") === "DOCTOR") {
                                            return <Caregivers />;
                                        } else {
                                            return <UnauthorizedPage />;
                                        }
                                    }}
                                />

                                <Route
                                    exact
                                    path="/medications"
                                    render={() => {
                                        if (localStorage.getItem("role") === "DOCTOR") {
                                            return <Medications />;
                                        } else {
                                            return <UnauthorizedPage />;
                                        }
                                    }}
                                />
                                <Route
                                    exact
                                    path="/caregiver"
                                    render={() => {
                                        if (localStorage.getItem("role") === "CAREGIVER") {
                                            return <CaregiverPage />;
                                        } else {
                                            return <UnauthorizedPage />;
                                        }
                                    }}
                                />
                                {/*Error*/}
                                <Route exact path="/error" render={() => <ErrorPage />} />
                                <Route
                                    exact
                                    path="/medicationPlan"
                                    render={() => {
                                        if (localStorage.getItem("role") === "DOCTOR") {
                                            return <MedicationPlans />;
                                        } else {
                                            return <UnauthorizedPage />;
                                        }
                                    }}
                                />

                                <Route
                                    exact
                                    path="/patientPage"
                                    render={() => {
                                        if (localStorage.getItem("role") === "PATIENT") {
                                            return <PatientRoles />;
                                        } else {
                                            return <UnauthorizedPage />;
                                        }
                                    }}
                                />
                                <Route render={() => <ErrorPage />} />
                            </Switch>
                        </div>
                    </Router>
                </div>
            </CookiesProvider>
        );
    }
}

export default App;
